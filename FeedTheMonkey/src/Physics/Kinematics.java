/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Physics;

/**
 *
 * @author caldw
 */
public class Kinematics {
    
    static final double GRAVITY = -9.80665;
    
    private static double findTimeToMonkey(Projectile projectile) {
        return projectile.getDistance() / projectile.getInitialXVelocity();
    }//findTime
    
    public static double collisionTime(Projectile projectile) {
        return findTimeToMonkey(projectile);
    }//collisionTime
    
    public static double findProjectileFinalYPosition(Projectile projectile) {
        double projectileInitialHeight = projectile.getLaunchHeight();
        double velocityContribution = projectile.getDistance() * Math.tan(projectile.getAngleInRadians());
        double accelerationContribution = findTimeToMonkey(projectile) * findTimeToMonkey(projectile) * GRAVITY / 2.0;
        return projectileInitialHeight + velocityContribution + accelerationContribution;
    }//findProjectileFinalYPosition
    
    public static double findMonkeyFinalYPosition(Projectile projectile, Monkey monkey) {
        double accelerationContribution = findTimeToMonkey(projectile) * findTimeToMonkey(projectile) * GRAVITY / 2.0;
        return monkey.getCeilingHeight() + accelerationContribution;
    }//findMonkeyFinalYPosition
    
    public static boolean isTheMonkeyHit(Projectile projectile, Monkey monkey) {
        double lowerBound = monkey.getLength() - monkey.getCeilingHeight() + projectile.getLaunchHeight() + projectile.getDistance()*Math.tan(projectile.getAngleInRadians());
        double upperBound = monkey.getCeilingHeight() -  projectile.getLaunchHeight() - projectile.getDistance()*Math.tan(projectile.getAngleInRadians());
        return (0.001 < lowerBound) && (0.001 < upperBound) && !limitingCases(projectile, monkey);
    }//isTheMonkeyHit
    
    public static String isTheMonkeyHitString(Projectile projectile, Monkey monkey) {
        return isTheMonkeyHit(projectile, monkey) ? "Yes" : "No";
    }//isTheMonkeyHitString
    
    private static boolean limitingCases(Projectile projectile, Monkey monkey) {
        double projectileFinalYPosition = findProjectileFinalYPosition(projectile);
        double monkeyFinalYPosition = findMonkeyFinalYPosition(projectile, monkey);
        //if(monkey has already landed)
        if (monkeyFinalYPosition - monkey.getLength() <= 0) { return !returnError("Monkey has landed already!"); }
        //if(projectile has hit the floor)
        if (projectileFinalYPosition < 0) { return !returnError("Projectile has hit the floor"); }
        return false;
    }//limitingCases
    
    private static boolean returnError(String error) {
        printError(error);
        return false;
    }//returnError
    
    private static void printError(String error) {
        System.err.println(error);
    }//printError
    
    public static void isTheMonkeyHitVerbose(Projectile projectile, Monkey monkey) {
        double monkeyFinalTop = Kinematics.findMonkeyFinalYPosition(projectile, monkey);
        double monkeyFinalBottom = monkeyFinalTop - monkey.getLength();
        double projectileFinalPosition = Kinematics.findProjectileFinalYPosition(projectile);
        System.err.println("---");
        System.err.println("Is the Monkey Hit? " + isTheMonkeyHitString(projectile, monkey));
        System.err.println("Time to \"Collision\": " + collisionTime(projectile));
        System.err.println("Monkey Bottom  <  Projectile  <  Monkey Top");
        System.err.println(monkeyFinalBottom + "  <  " + projectileFinalPosition + "  <  " + monkeyFinalTop);
        System.err.println("---");
    }//isTheMonkeyHitVerbose
    
    private static double angleAtMonkey(double distance, double launchHeight, double ceilingHeight, double monkeyLength) {
        return Math.atan((ceilingHeight - launchHeight - 0.5*monkeyLength) / distance) * 180 / Math.PI;
    }//angleAtMonkey
    
    public static double angleAtMonkey(double distance, double launchHeight, Monkey monkey) {
        return angleAtMonkey(distance, launchHeight, monkey.getCeilingHeight(), monkey.getLength());
    }//angleAtMonkey
    
    public static void main(String args[]) {
        double ceilingHeight = 3.25;//meters #chosen to be distance+launch height+.5monkey length
        double length = 0.1;//meters
        Monkey monkey = new Monkey(ceilingHeight, length);
        System.err.println(monkey.toString());
        double angle = 45.0; //degrees
        double initialVelocity = 6.0; //meters per second
        double launchHeight = 0.2; //meters
        double distance = 3.0;//meters
        Projectile projectile = new Projectile(angle, initialVelocity, launchHeight, distance);
        System.err.println(projectile.toString());
        isTheMonkeyHitVerbose(projectile, monkey);
        projectile.setAngle(45.5);
        isTheMonkeyHitVerbose(projectile, monkey);
        projectile.setAngle(44.5);
        isTheMonkeyHitVerbose(projectile, monkey);
    }//main
}

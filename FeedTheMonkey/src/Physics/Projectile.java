/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Physics;

/**
 *
 * @author caldw
 */
public class Projectile {
    
    private double angle, initialVelocity, launchHeight, distance;
    
    public Projectile() {
        angle = 0;
        initialVelocity = 0;
        launchHeight = 0;
        distance = 0;
    }//Projectile constructor(basic)
    
    public Projectile(double angle, double initialVelocity, double launchHeight, double distance) {
        this.angle = angle;
        this.initialVelocity = initialVelocity;
        this.launchHeight = launchHeight;
        this.distance = distance;
    }//Projectile constructor

    public void setInitialVelocity(double initialVelocity) {
        this.initialVelocity = initialVelocity;
    }

    public void setLaunchHeight(double launchHeight) {
        this.launchHeight = launchHeight;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
    
    public double getLaunchHeight() {
        return launchHeight;
    }
 
    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
    
    public double getDistance() {
        return distance;
    }
    
    public double getInitialVelocity() {
        return initialVelocity;
    }
   
    public double getAngleInRadians() {
        return this.getAngle()*Math.PI/180.0;
    }//getAngleInRadians
    
    public double getInitialXVelocity() {
        return getInitialVelocity()*Math.cos(this.getAngleInRadians());
    }//getInitialXVelocity
    
    public double getInitialYVelocity() {
        return getInitialVelocity()*Math.sin(this.getAngleInRadians());
    }//getInitialYVelocity
    
    public boolean beenInitialized() {
        return !(0 == angle && angle == initialVelocity && initialVelocity == launchHeight && launchHeight == distance);
    }//beenInitialized
    
    @Override
    public String toString() {
        String str = "Projectile:\n----------\n";
        str += "Angle: " + Double.toString(this.getAngle()) + " degrees\n";
        str += "Angle: " + Double.toString(this.getAngleInRadians()) + " radians\n";
        str += "Launch Height: " + Double.toString(this.getLaunchHeight()) + " meters\n";
        str += "Distance: " + Double.toString(this.getDistance()) + " meters\n";
        str += "Initial Velocity: " + Double.toString(this.getInitialVelocity()) + " meters per second\n";
        str += "Initial X Velocity: " + Double.toString(this.getInitialXVelocity()) + " meters per second\n";
        str += "Initial Y Velocity: " + Double.toString(this.getInitialYVelocity()) + " meters per second\n";
        return str;
    }//toString
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Physics;

/**
 *
 * @author caldw
 */
public class Monkey {
    
    private double ceilingHeight, length;
    
    public Monkey() {
       ceilingHeight = 0;
       length = 0;
    }//Monkey constructor(basic)
    
    public Monkey(double ceilingHeight, double length) {
        this.ceilingHeight = ceilingHeight;
        this.length = length;
    }//Monkey constructor

    public void setCeilingHeight(double ceilingHeight) {
        this.ceilingHeight = ceilingHeight;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getCeilingHeight() {
        return ceilingHeight;
    }

    public double getLength() {
        return length;
    }
    
    public boolean beenInitialized() {
        return !(0 == ceilingHeight && ceilingHeight == length);
    }//beenInitialized
    
    @Override
    public String toString() {
        String str = "Monkey:\n------\n";
        str += "Ceiling Height: " + Double.toString(this.getCeilingHeight()) + " meters\n";
        str += "Length of Monkey: " + Double.toString(this.getLength()) + " meters\n";
        return str;
    }//printString
    
}

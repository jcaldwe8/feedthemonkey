/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import Interface.Menu;
import Physics.Kinematics;
import Physics.Monkey;
import Physics.Projectile;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Scanner;

/**
 *
 * @author caldw
 */
public class FeedTheMonkey {
    
    protected static double angleAtMonkey;
    
    public static void main(String[] args) {
        Monkey monkey = new Monkey();
        Projectile projectile = new Projectile();
        System.err.println("Working Directory = " + System.getProperty("user.dir"));
        if (args.length == 0 || !("m".equalsIgnoreCase(args[0]) || "c".equalsIgnoreCase(args[0]))) {
            System.err.println("This program is run with this format:");
            System.err.println("java FeedTheMonkey {interactive mode(m) or command line(c)} {optional:file name}");
            System.exit(0);
        }//if (no arguments provided)
        System.err.println("Arguments provided:");
        for(String arg : args) {
            System.err.println(arg);
        }//for-each
        if (args.length < 2 && "m".equalsIgnoreCase(args[0])) {
            makeMonkey(monkey);
            makeProjectile(monkey,projectile);
            runSimulation(monkey, projectile);
        } else if (args.length < 2 && "c".equalsIgnoreCase(args[0])) {
            //run interface
            new Menu().setVisible(true);
        } else {
            String filename = args[1];
            getDataFromFile(filename, monkey, projectile);
            runSimulation(monkey, projectile);
        }//if (filename has been provided)
        System.err.println("Have a nice day!");
    }//main
    
    public static void makeMonkey(Monkey monkey) {
        Scanner scan = new Scanner(System.in);
        System.err.print("How high is the monkey being placed? (Measure from top) ");
        monkey.setCeilingHeight(scan.nextDouble());
        System.err.print("How tall is the monkey? ");
        monkey.setLength(scan.nextDouble()); 
    }//makeMonkey
    
    public static void makeMonkey(String line, Monkey monkey) {
        String[] values = line.split(",");
        monkey.setCeilingHeight(Double.valueOf(values[0]));
        monkey.setLength(Double.valueOf(values[1]));
        System.err.println("Monkey set with values:\nCeiling Height: " + values[0] + "\nLength: " + values[1]);
    }//makeMonkey
    
    public static double getAngleAtMonkey(Monkey monkey, double distance) {
        double angle, monkeyCenterOfMassHeight;
        monkeyCenterOfMassHeight = monkey.getCeilingHeight() - 0.5*monkey.getLength();
        angle = Math.atan(monkeyCenterOfMassHeight / distance);
        return angle;
    }//getAngle
    
    public static void makeProjectile(Monkey monkey, Projectile projectile) {
        Scanner scan = new Scanner(System.in);
//        double angle, height, initialVelocity, distance;
        System.err.print("How far off the ground is the projectile at launch? ");
        projectile.setLaunchHeight(scan.nextDouble());
        System.err.print("How fast is the projectile at launch? ");
        projectile.setInitialVelocity(scan.nextDouble());
        System.err.print("How far away from the monkey is the projectile? ");
        projectile.setDistance(scan.nextDouble());
        //angleAtMonkey = getAngleAtMonkey(monkey, distance);
        angleAtMonkey = Kinematics.angleAtMonkey(projectile.getDistance(), projectile.getLaunchHeight(), monkey);
        System.err.println("The angle directly at the monkey is: " + angleAtMonkey);
        System.err.print("What should the first angle be? ");
        projectile.setAngle(scan.nextDouble());
    }//makeProjectile
    
    public static void makeProjectile(String line, Monkey monkey, Projectile projectile) {
        String[] values = line.split(",");
        projectile.setAngle(Double.valueOf(values[0]));
        projectile.setInitialVelocity(Double.valueOf(values[1]));
        projectile.setLaunchHeight(Double.valueOf(values[2]));
        projectile.setDistance(Double.valueOf(values[3]));
        System.err.println("Projectile set with values:\nAngle: " + values[0] + "\nInitial Velocity: " + values[1] + "\nLaunch Height: " + values[2] + "\nDistance: " + values[3]);
    }//makeProjectile
    
    protected static void changeAngle(Projectile projectile) {
        Scanner scan = new Scanner(System.in);
        double newAngle;
        System.err.println("The current angle is " + projectile.getAngle());
        System.err.println("The angle at the monkey is " + angleAtMonkey);
        System.err.print("What do you want the new angle to be? ");
        newAngle = scan.nextDouble();
        projectile.setAngle(newAngle);
        if (!(Math.abs(projectile.getAngle() - newAngle) < 0.001)) {
            System.err.println("An error has occurred trying to change the angle - please try again");
            return;
        }//
        System.err.println("The angle has been changed to " + projectile.getAngle() + " degrees");
    }//changeAngle
    
    protected static void fireProjectile(Monkey monkey, Projectile projectile) {       
        Kinematics.isTheMonkeyHitVerbose(projectile, monkey);
    }//fireProjectile
    
    protected static void getDataFromFile(String filename, Monkey monkey, Projectile projectile) { 
        LineNumberReader reader = null;
        try {
            reader = new LineNumberReader(new FileReader(".\\models\\" + filename));
            System.err.println("Loading ./model/" + filename + "...");
            makeMonkey(reader.readLine(), monkey);
            makeProjectile(reader.readLine(), monkey, projectile);
        } catch (Exception ex){
            System.err.println("File Not Found!!!\nMake sure that " + filename + " exists!");
            ex.printStackTrace();
            System.exit(0);
        } finally {
            //Close the LineNumberReader
            try {
                if (reader != null){
                    reader.close();
                }
            } catch (IOException ex){
                ex.printStackTrace();
            }//try-catch
        }//try-catch-finally
    }//getDataFromFile
    
    protected static void saveDataToFile(Monkey monkey, Projectile projectile) {
        
    }//saveDataToFile
    
    public static void runSimulation(Monkey monkey, Projectile projectile) {
        Scanner scan = new Scanner(System.in);
        String action;
        System.err.print("Would you like to fire (F), change the angle (C), or end the simulation (E)? ");
        action = scan.next();
        action = action.toUpperCase();
        System.err.println("Option chosen: " + action);
        while (!action.equals("E") && !action.equals("END")) {
            switch(action) {
            case "C":
            case "CHANGE":
                changeAngle(projectile);
                break;
            case "F":
            case "FIRE":
                fireProjectile(monkey, projectile);
                break;
            default:
                System.err.println("Please enter a valid option from the following:");
            }//switch(action)
            System.err.print("Fire (F) | Change the angle (C) | End the simulation (E): ");
            action = scan.next();
            action = action.toUpperCase();
            System.err.println("Option chosen: " + action);
        }//
    }//runSimulation
}//main
